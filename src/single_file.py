import sys
import subprocess
import os

sys.path.append('src')

class SourceFile:
    """
    >>> SourceFile().contents
    []
    >>> getattr(SourceFile(dummy=True).contents[-1],'doc')
    ['dummy documentation']
    """
    def __init__(self, ftype=None, filepath=None, dummy=False):
        self.filepath = filepath
        self.ftype = ftype
        self.contents = [Element(dummy=True)] if dummy else []
        self.doc = [] if dummy else []
        self.test = [] if dummy else []

class Element:
    """
    >>> Element(categ='foo').categ
    'foo'
    """
    def __init__(self, name='', categ='', dummy=False):
        self.name = name
        self.categ = categ
        self.type = 'dummy' if dummy else None 
        self.doc = ['dummy documentation'] if dummy else []
        self.test = ['dummy test'] if dummy else []


        

def mkdir(path):
    """
    Example:
    >>> subprocess.call('rm -fr test/testdir',shell=True); mkdir('test/testdir'); subprocess.call('ls test/testdir',shell=True)
    0
    0

    """
    subprocess.call('mkdir -p '+path,shell=True)

def init_docfile(pfile):
    filename = get_filename('doc',pfile.filepath)

def finl_docfile(pfile):
    filename = get_filename('doc',pfile.filepath)

def init_testfile(pfile):
    filename = get_filename('test',pfile.filepath)
    with open(filename,'a') as f:
        f.write("module mod_test_"+get_filetitle(pfile.filepath)+'\n')
        f.write('use redpen_lib\n')
        f.write('use '+get_filesubpath(pfile.filepath).split('.')[0]+'\n')
        f.write('contains'+'\n')

def finl_testfile(pfile):
    filename = get_filename('test',pfile.filepath)
    with open(filename,'a') as f:
        f.write("end module mod_test_"+get_filetitle(pfile.filepath)+'\n')



def get_rootdir(filepath):
    return (lambda x: '/'.join(x[:x.index('src')]))(filepath.split('/'))

def get_filename(kind,filepath):
    """
    Example:
    >>> get_filename('doc','where/what/src/subsrc/file.ext')
    'where/what/doc/doc_subsrc_file.md'
    >>> get_filename('test','test/src/hello')
    'test/test/test_hello.f90'
    """
    rootdir = get_rootdir(filepath)
    subpath = kind+"_"+get_filetitle(filepath)+{
            'doc': '.md', 'test': '.f90'
            }[kind]
    mkdir(rootdir+'/'+kind)
    return rootdir+'/'+kind+'/'+subpath#.replace('/','_')

def get_filesubpath(filepath):
    return (lambda x: '/'.join(x[x.index('src')+1:]))(filepath.split('/'))

def get_filetitle(filepath):
    """ 
    Example:
    >>> get_filetitle('where/what/src/subsrc/file.ext')
    'subsrc_file'
    >>> get_filetitle('test/src/hello')
    'hello'
    """
    subpath = get_filesubpath(filepath)
    return subpath.split('.')[0].replace('/','_')





def line_kind(l):
    """ 
    returns the kind (test/doc/code) of a given line (string)

    Example:
    >>> line_kind('!> test')
    'test'
    >>> line_kind('![ doc')
    'doc'
    >>> line_kind('! comment')
    'code'
    >>> line_kind('subroutine dummy')
    'code'
    """
    return {
            '!>': 'test',
            '![': 'doc'
            }.get(l[0:2].strip(),'code')

    
def find_next_line(l,lines,kind,direction='down'):
    """
    returns the index of the next line (starting from `l`) and the line itself, of the given `kind` in `lines` (`direction` is default to 'down', but can be 'up')

    Example:
    >>> lines = ['subroutine','![ what it does','!> run test','integer']; find_next_line(lines[1],lines,'code')
    (3, 'integer')
    >>> lines = ['subroutine','![ what it does','!> run test','integer']; find_next_line(lines[0],lines,'doc',direction='down')
    (1, '![ what it does')
    >>> lines = ['subroutine','![ what it does','!> run test','integer']; find_next_line(lines[1],lines,'code',direction='up')
    (0, 'subroutine')
    """
    i0 = lines.index(l)
    rng = range(i0,{'up':-1,'down':len(lines)}.get(direction),
            {'up':-1,'down':+1}.get(direction))
    for i in rng:
        if(line_kind(lines[i]) == kind):
            return i,lines[i]



def parse_line(l,kind,title='no',caller=None):
    """
    testing title top: 'subroutine _name_of_block_to_be_tested_'
    testing title bottom: 'end subroutine _name_of_block_to_be_tested_'
    documentation title top: '# subroutine _name_of_block_to_be_tested_'
    documentation title bottom: ''

    >>> parse_line('subroutine hello(a,b,c)','test',title='top')
    'subroutine test_hello\\n'
    >>> parse_line('subroutine hello(a,b,c)','test',title='bottom')
    'end subroutine test_hello\\n\\n'
    >>> parse_line('subroutine hello(a,b,c)','doc',title='top')
    '\\n# subroutine hello\\n'
    >>> parse_line('subroutine hello(a,b,c)','doc',title='bottom')
    >>> parse_line('subroutine prova','doc',title='top')
    '\\n# subroutine prova\\n'
    >>> parse_line('!> call rp_assert(a == b+c)','test',caller='fort_subroutine')
    'call rp_assert(a == b+c,"a == b+c","fort_subroutine")\\n'
    >>> parse_line('!>a = 111','test')
    'a = 111'
    """
    if(title == 'top'):
        if(kind == 'test'):
            return 'subroutine test_'+(l.split('(')[0].split()[1])+'\n'
        elif(kind == 'doc'):
            return '\n# '+' '.join(l.split('(')[0].split()[:2])+'\n'
        else:
            return 
    if(title == 'bottom'):
        if(kind == 'test'):
            return 'end subroutine test_'+(l.split('(')[0].split()[1])+'\n\n'
        else:
            return 
    else:
        parsed = l[2:]
        if 'rp_assert' in parsed:
            #elements = parsed.split('(')[0].replace('/','_')
            assertion = parsed.split('(')[1].split(')')[0]           
            parsed = "call rp_assert("+assertion+',\"'+assertion+'\",\"'+caller+'\")\n'
        return parsed


def action_write(pfile,l,kind,filepath,title=None):
    """
    write a line: `kind` specifies if it is for the documentation or for the testing code; `title` specifies if the line should be parsed in order to serve as title

    >>> import subprocess; subprocess.call('mkdir -p test/doc',shell=True); action_write(SourceFile(dummy=True),'title line','doc','test/src/docfile',title='top')
    0
    'test/doc/doc_docfile.md'
    """
    parsedline = parse_line(l,kind,title=title,caller=get_filetitle(filepath))
    filename = get_filename(kind,filepath)
    with open(filename,'a') as f:
        getattr(pfile.contents[-1],kind).append(parsedline)
    return filename



def add_element(pfile,nameline):
    """
    >>> add_element(SourceFile(),'   subroutine   ciao').contents[-1].name
    'ciao'
    >>> add_element(SourceFile(),'   function   ciao(a , f)').contents[-1].categ
    'function'
    >>> sf = SourceFile(); ret = add_element(sf,'subroutine whatsmyname'); ret = add_element(sf,'   subroutine whatsmyname'); len(sf.contents)
    1
    """
    typ =  nameline.split('(')[0].split()[0]
    name = nameline.split('(')[0].split()[1]
    if name not in [el.name for el in pfile.contents]:
        if(typ in ['subroutine','function','module']):
            pfile.contents.append(Element(name=name,categ=typ)) 
    return pfile




def action_line(pfile,l,lines,filepath):
    """
    execute action on a line

    Example:
    >>> action_line(SourceFile(dummy=True),'![ wrongdoc',['![ wrongdoc','subroutine','![ rightdoc'],'test/none/src')
    'Error: First line should not be documentation!'
    >>> action_line(SourceFile(dummy=True),'!> wrongtest',['!> wrongtest','subroutine','!> righttest'],'test/none/src')
    'Error: First line should not be testing code!'
    >>> action_line(SourceFile(dummy=True),'!> test line',['subroutine prova','!> doc line','!> test line','!> other test line'],'test/none/src/test_action_test')[0].contents[0].test
    ['dummy test', ' test line']
    >>> action_line(SourceFile(dummy=True),'!> test line',['subroutine whatsmyname','![ doc line','!> test line','!> other test line'],'test/none/src/test_action_test')[0].contents[-1].name
    'whatsmyname'
    """
    kind = line_kind(l) 
    if(kind == 'code'):
        return 'code line'
    il = lines.index(l)
    if(il==0):
        return "Error: First line should not be "+{
                'doc':'documentation','test':'testing code'}[kind]+"!"
    nameline = None
    dbglines = []
    if(line_kind(lines[il-1]) != kind):
        nameline = find_next_line(l,lines,'code',direction='up')[1]
        pfile = add_element(pfile,nameline)
        action_write(pfile,nameline,kind,filepath,title='top')
    dbglines.append((l,lines[il-1]))
    action_write(pfile,l,kind,filepath)
    if(kind == 'test'):
        if(il<(len(lines)-1) and line_kind(lines[il+1]) != 'test'):
            action_write(pfile,find_next_line(l,lines,'code',direction='up')[1],kind,filepath,title='bottom')
    return [pfile]


def walk_over_lines(pfile,lines,filepath):
    """
    iterate over lines and calls the action

    Example:
    >>> walk_over_lines(SourceFile(),['subroutine hello','![ documenting hello','![ still documenting hello','!> testing hello','!> still testing hello','end subroutine hello','subroutine hi','![ documenting hi','![ still documenting hi','!> testing hi','!> still testing hi','end subroutine hi'],'test/src/hello').contents[0].categ
    'subroutine'
    >>> walk_over_lines(SourceFile(),['subroutine hello','![ documenting hello','![ still documenting hello','!> testing hello','!> still testing hello','end subroutine hello','subroutine hi','![ documenting hi','![ still documenting hi','!> testing hi','!> still testing hi','end subroutine hi'],'test/src/hello').contents[0].test
    ['subroutine test_hello\\n', ' testing hello', ' still testing hello', 'end subroutine test_hello\\n\\n']
    """
    for l in lines:
        action_line(pfile,l,lines,filepath)
    return pfile


def process_file(filepath):
    pfile = SourceFile(filepath=filepath)

    subprocess.call('rm -f '+get_filename('doc',filepath),shell=True)
    subprocess.call('rm -f '+get_filename('test',filepath),shell=True)

    with open(filepath) as f:
        walk_over_lines(pfile,[l.strip() for l in f.readlines()],filepath)

    init_docfile(pfile)
    with open(get_filename('doc',pfile.filepath),'a') as f:
        for c in pfile.contents:
            for l in c.doc:
                f.write(l+'\n')
    finl_docfile(pfile)
    init_testfile(pfile)
    with open(get_filename('test',pfile.filepath),'a') as f:
        for c in pfile.contents:
            for l in c.test:
                f.write(l+'\n')
    finl_testfile(pfile)
    return pfile


if __name__ == "__main__":
    if(len(sys.argv)<2 or sys.argv[1] == 'test'):
        import doctest
        subprocess.call('rm -rf test',shell=True)
        doctest.testmod()
        subprocess.call('rm -rf test',shell=True)
    else: 
        process_file(sys.argv[1])

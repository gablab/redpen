module redpen_lib

contains
  subroutine rp_assert(assertion,container,title)
    logical, intent(in) :: assertion
    character(len=*), intent(in) :: container, title
    if(assertion) then
    else
      write(*,*) "Error! "//trim(container)//": "//trim(title)
    end if
  end subroutine rp_assert


end module redpen_lib

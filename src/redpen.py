import sys
import subprocess
import os
import json

sys.path.append('src')
from single_file import process_file, mkdir, get_filetitle, get_rootdir


def makefile(dirpath,titles):
    dependencies = json.load(open(get_rootdir(dirpath)+"/dependencies.json"))
    lines_makefile = []
    lines_makefile.append("VPATH = "+' '.join([p for p in dependencies['fortran_files']['paths']])+"\n")
    lines_makefile.append("FILES = redpen_lib.f90")
    for d in dependencies['fortran_files']['files']:
        lines_makefile.append(' '+d)
    for t in titles:
        lines_makefile.extend([' '+t+'.f90',' test_'+t+'.f90'])
    lines_makefile.append(' main.f90')
    lines_makefile = [l.replace('.f90','.o') for l in lines_makefile]
    with open("src/template_makefile","r") as f:
        lines_makefile.extend(f.readlines())
    with open(get_rootdir(dirpath)+'/test/Makefile','w') as f:
        f.write(''.join(lines_makefile))



def process_dir(dirpath):
    titles = []
    testdir = get_rootdir(dirpath)+'/test'
    docdir = get_rootdir(dirpath)+'/doc'
    subprocess.call('rm -rf '+testdir+'/*',shell=True)
    subprocess.call('rm -rf '+docdir+'/*',shell=True)
    mkdir(testdir)
    mkdir(docdir)
    subprocess.call('cp src/lib/redpen_lib.f90 '+testdir,shell=True)
    subprocess.call('cp src/README.md '+testdir,shell=True)
    subprocess.call('cp src/README.md '+docdir,shell=True)
    mainpath = testdir+'/main.f90'
    pfiles = []
    with open(mainpath,'a') as fmain:
        fmain.write('program main_test'+'\n\n')
        fmain.write('use redpen_lib\n')
    for f in os.listdir(dirpath):
        if (f[-4:]=='.f90'):
            filepath = dirpath+'/'+f
            subprocess.call('cp '+filepath+' '+testdir,shell=True)
            print(filepath)
            pfiles.append(process_file(filepath))
            with open(filepath) as fh:
                title = fh.readlines()[0].split()[1]
                titles.append(title)
    with open(mainpath,'a') as fmain:
        for t in titles:
            fmain.write('use '+t+'\n')
            fmain.write('use mod_test_'+t+'\n')
        for pf in pfiles:
            for el in pf.contents:
                fmain.write('call test_'+el.name+'\n')

        fmain.write('\nend program main_test')
    makefile(dirpath,titles)


if __name__ == "__main__":
    if(len(sys.argv)<2 or sys.argv[1] == 'test'):
        import doctest
        subprocess.call('rm -rf test',shell=True)
        doctest.testmod()
        subprocess.call('rm -rf test',shell=True)
    else: 
        process_dir(sys.argv[1])


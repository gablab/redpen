# redpen
(**works only with modules**)
For testing 
`python3 src/redpen.py test`

For a sample
`py ./src/redpen.py sample`

## Rules
- first line should be `module modulename`;
- call `rp_assert(condition)` in the source file to test: the program will produce the complete form visible in src/lib/redpen_lib.f90;
- don't use parentheses in arguments of rp_assert;


module prova_mod

contains
  subroutine trysum(A,B,C,o)
    ![ takes three integers to 
    ![ calculate their sum as a real
    !> real :: out
    !> call trysum(1,2,3,out)
    !> call rp_assert(out .eq. 6.0)
    implicit none
    integer, intent(in) :: A,B,C
    ![_ A
    ![ first integer
    ![_ B
    ![ second integer
    ![_ C
    ![ third integer
    real, intent(out) :: o
    ![_ o
    ![ the output parameter
    o = real(A+B+C)
  end subroutine trysum

  function tryrms(A,B,C) result(y)
    ![ takes two integers to
    ![ calculate their product
    !> double precision :: out, cmpr
    !> out = tryrms(4.0d0,10.0d0,16.0d0)
    !> cmpr = sqrt(2*6**2/3.)
    !> call rp_assert(out -cmpr .lt. 10e-5)
    use modStats
    implicit none
    double precision, intent(in) :: A,B,C
    double precision, dimension(3) :: arr,marr
    ![_ A
    ![ first real
    ![_ B
    ![ second real
    ![_ C
    ![ third real
    double precision :: y
    arr = (/ A, B, C /)
    marr = sum(arr)/3.
    print*,arr,marr
    y = mean_root_difference(arr, marr)
  end function tryrms
end module prova_mod

module tentativo_mod
contains

  subroutine attemptsum(A,B,C,o)
    ![ takes three integers to 
    ![ calculate their sum as a real
    !> real :: out
    !> call attemptsum(1,2,3,out)
    !> call rp_assert(out .eq. 6.0)
    implicit none
    integer, intent(in) :: A,B,C
    ![_ A
    ![ first integer
    ![_ B
    ![ second integer
    ![_ C
    ![ third integer
    real, intent(out) :: o
    ![_ o
    ![ the output parameter
    o = real(A+B+C)
  end subroutine attemptsum

  function attemptmult(A,B) result(y)
    ![ takes two integers to
    ![ calculate their product
    !> integer :: out
    !> out = attemptmult(3,4)
    !> call rp_assert(out .eq. 12)
    implicit none
    integer, intent(in) :: A,B
    ![_ A
    ![ first integer
    ![_ B
    ![ second integer
    integer :: y
    y = A*B
  end function attemptmult

end module tentativo_mod

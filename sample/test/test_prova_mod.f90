module mod_test_prova_mod
use redpen_lib
use prova_mod
contains
subroutine test_trysum

 real :: out
 call trysum(1,2,3,out)
call rp_assert(out .eq. 6.0,"out .eq. 6.0","prova_mod")

end subroutine test_trysum


subroutine test_tryrms

 double precision :: out, cmpr
 out = tryrms(4.0d0,10.0d0,16.0d0)
 cmpr = sqrt(2*6**2/3.)
 print*,out,cmpr
call rp_assert(out -cmpr .lt. 10e-5,"out -cmpr .lt. 10e-5","prova_mod")

end subroutine test_tryrms


end module mod_test_prova_mod

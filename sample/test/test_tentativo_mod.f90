module mod_test_tentativo_mod
use redpen_lib
use tentativo_mod
contains
subroutine test_attemptsum

 real :: out
 call attemptsum(1,2,3,out)
call rp_assert(out .eq. 6.0,"out .eq. 6.0","tentativo_mod")

end subroutine test_attemptsum


subroutine test_attemptmult

 integer :: out
 out = attemptmult(3,4)
call rp_assert(out .eq. 12,"out .eq. 12","tentativo_mod")

end subroutine test_attemptmult


end module mod_test_tentativo_mod

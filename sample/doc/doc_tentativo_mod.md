
# subroutine attemptsum

 takes three integers to
 calculate their sum as a real

# integer, intent

_ A
 first integer
_ B
 second integer
_ C
 third integer

# real, intent

_ o
 the output parameter

# function attemptmult

 takes two integers to
 calculate their product

# integer, intent

_ A
 first integer
_ B
 second integer
